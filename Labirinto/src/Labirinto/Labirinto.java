package Labirinto;

public class Labirinto {
	//variaveis do jogo
	private static final char PRD_VRT = '|';
	
	private static final char PRD_HRZ = '_';
	
	private static final char VAZIO = ' ';
	
	private static final char PRD_INTR = '#';
	
	private static final char TAMANHO = 10;
	
	private static final double PROB = 0.6;

	private static char[][] tabuleiro;
	
	private static final char INICIO = 'I';

	private static final char FIM = 'F';

	private static int linhaInicio;
	
	private static int colunaInicio;
	
	private static final char CAMINHO = '�';

	private static final char SEM_SAIDA = '-';




	//codigo do jogo
	//define o tabuleiro
	public static void inicializarMatriz() {

	      int i, j;

	      for (i = 0; i < TAMANHO; i++) {
	    	  tabuleiro[i][0] = PRD_VRT;
		      tabuleiro[i][TAMANHO - 1] = PRD_VRT;
		      tabuleiro[0][i] = PRD_HRZ;
		      tabuleiro[TAMANHO - 1][i] = PRD_HRZ;
		      }
		      for (i = 1; i < TAMANHO - 1; i++) {
		         for (j = 1; j < TAMANHO - 1; j++) {
		            tabuleiro[i][j] = VAZIO;
		         }
		      }
		      //coloca as parecdes internas aleatoriamanete
		   for (i = 1; i < TAMANHO - 1; i++) {		  
			   for (j = 1; j < TAMANHO - 1; j++) {
		            if (Math.random() > PROB) {
		    	           tabuleiro[i][j] = PRD_INTR;
		 	      } else {
		    	           tabuleiro[i][j] = VAZIO;
		 	      }
			   }
		   }
	}
		   
	//imprime o tabuleiro
	public static void imprimir() {
		      for (int i = 0; i < TAMANHO; i++) {
		         for (int j = 0; j < TAMANHO; j++) {
		            System.out.print(tabuleiro[i][j]);
		         }
		         System.out.println();
		      }
		   }
	
	public static int gerarNumero(int minimo, int maximo) {
		   int valor = (int) Math.round(Math.random()  * (maximo - minimo));
		   return minimo + valor;
		}
	
	public static boolean procurarCaminho(int linhaAtual, int colunaAtual) {
		   int proxLinha;
		   int proxColuna;
		   boolean achou = false;
		   // tenta subir
		   proxLinha = linhaAtual + 1;
		   proxColuna = colunaAtual;
		   achou = tentarCaminho(proxLinha, proxColuna);
		   // tenta descer
		   if (!achou) {
		      proxLinha = linhaAtual - 1;
		      proxColuna = colunaAtual;
		      achou = tentarCaminho(proxLinha, proxColuna);
		   }
		   // tenta � esquerda
		   if (!achou) {
		      proxLinha = linhaAtual;
		      proxColuna = colunaAtual + 1;
		      achou = tentarCaminho(proxLinha, proxColuna);
		   }
		   // tenta � direita
		   if (!achou) {
		      proxLinha = linhaAtual;
		      proxColuna = colunaAtual - 1;
		      achou = tentarCaminho(proxLinha, proxColuna);
		   }
		   return achou;
		}

	private static boolean tentarCaminho(int proxLinha, int proxColuna) {
		   boolean achou = false;
		   if (tabuleiro[proxLinha][proxColuna] == FIM) {
		      achou = true;
		   } else if (posicaoVazia(proxLinha, proxColuna)) {
		      // marcar
		      tabuleiro[proxLinha][proxColuna] = CAMINHO;
		      imprimir();
		      achou = procurarCaminho(proxLinha, proxColuna);
		    if (!achou) {
		         tabuleiro[proxLinha][proxColuna] = SEM_SAIDA;
		         imprimir();
		      }
		   }
		   return achou;
		}
	
	public static boolean posicaoVazia(int linha, int coluna) {
		   boolean vazio = false;
		   if (linha >= 0 && coluna >= 0 && linha < TAMANHO && coluna < TAMANHO) {
		      vazio = (tabuleiro[linha][coluna] == VAZIO);
		   }
		   return vazio;
		}



	//main do jogo
	public static void main(String Arg[]) {
		      tabuleiro = new char[TAMANHO][TAMANHO];

		      inicializarMatriz();
		      linhaInicio = gerarNumero(TAMANHO - 2, TAMANHO - 2);
		      colunaInicio = gerarNumero(TAMANHO - 2, TAMANHO - 2);
		      tabuleiro[linhaInicio][colunaInicio] = INICIO;
		      int linhaFim = gerarNumero(1 , TAMANHO / 2 - 1);
		      int colunaFim = gerarNumero(1 , TAMANHO / 2 - 1);
		      tabuleiro[linhaFim][colunaFim] = FIM;
		      imprimir();
		      
		      System.out.println("\n- Procurando solu��o -\n");
		      boolean achou = procurarCaminho(linhaInicio, colunaInicio);
		         if (achou) {
		            System.out.println("ACHOU O CAMINHO!");
		         } else {
		            System.out.println("N�o tem caminho!");
		         }
		         try {
						Thread.sleep(300);
		         } catch (InterruptedException e) {
						e.printStackTrace();
				 }
		      
		      
		     
		   }
	
			
   
		   
		}
